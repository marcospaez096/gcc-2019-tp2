# Documentacion 

En primer lugar se crea el archivo ".gitlab-ci.yml", dicho archivo se ejecuta cuando se hace push de un commit a un branch. Este archivo lo ejecutan los "Runners" que son maquinas virtuales aisladas.El archivo ".gitlab-ci.yml" define la estructrura y el orden del pipeline(el orden de ejecucion de un grupo de "jobs" que son  ejecutados en  "stages")


## Definicion de la imagen a utilizar
En primer lugar dentro del archivo ".gitlab-ci.yml" se define la imagen que se utiliza para la ejecucion de  los "jobs".Puesto que cada "job" se ejecuta dentro de un docker-container dentro de la maquina virtual del "Runner". Ya que la instalacion y los test estan hecho en nodejs y corren en docker-containers, estos docker-container deben de tener instalada la imagen de node en su ultima version. Asi:



```bash
image: "node:latest"
```

## Definicion de stages

Luego se definen los stages que indican el orden de ejecucion de los jobs .Ya que los jobs se ejecutan segun el stage en que fueron definidos.

```
#Se define los stages,que son los pasos por los cuales pasara la rama ante cualquier cambio. a un stage pueden pertenecer varios jobs o task.
stages: 
  - testeo
  - despliegue


```
Asi primero se ejecutaran los jobs que pertenezcan al stage de "build" , luego los jobs o task que pertenezcan al stage "testeo" y por ultimo los jobs que pertenezcan al stage "despliegue"

## Definicion de jobs
Los jobs son ejecutados por el Runner dentro de un docker-container y son tareas o task que van desde instalar dependencias , correr tests hasta realizar despliegue de aplicaciones




1. Primer Job, denominado test, se ejecuta en un  **Environment**  denominado "homologacion", agregandole el keyword **environment** al job "test", se puede especificar en que entorno se va a ejecutar este job o task y asi poder saber en donde el job se ejecuta.Este job pertence al primer stage(testeo), asi se ejecutara primero.Previo al test, se instalan las dependencias, en el mismo job.El test que se ejecuta consiste en la verificacion de un usuario en un archivo json , como tambien verifica el status code de un post request al login form
```
#Definicion del job "test", este job pertenece al primer stage "testeo", este job se realiza en el entorno "homologacion",dicho job ejecuta los scripts de instalacion de dependencias y tambien el script de test utilizando npm"
test:
  environment:
    name : homologacion
  script:
    - "npm install"
  #Se ejecuta la pruebas con mocha javascript
    - "npm test"
  stage: testeo



```

2. Segundo job, denominado "despliegue_en_entorno_de_desarrollo", se ejecuta en el **Environment** de **desarrollo**. Este job se ejecuta despues de la ejecucion de los jobs que pertenezcan al primer stage testeo, puesto que este job pertence al segundo stage(despliegue en este caso). Si dicho job se completa correctamente(con exito) este deployment es guardado(en el entorno especificado), ademas se habilita un boton al lado del entorno en el cual se ejecuto dicho job.Al oprimir dicho **boton**, nos lleva a la **app deployada** que se encuentra especificada en la **url**.Ademas este job solo se ejecuta cuando se realizan cambios en la **rama** de **desarrollo** , dicho comportamiento se encuentra definido por el **keyword** **"only"** definido con la palabra desarrollo(especifica que dicho job se ejecutara solo con la rama desarrollo).**Aqui el codigo en que se encuentra en la rama desarrollo se envia a heroku para su despliegue como una aplicacion en heroku**, la aplicacion en heroku se denomina **gcc-tp2-2019-desarrollo**, la url de app en desarrollo deployada es "**http://gcc-tp2-2019-desarrollo.herokuapp.com**"
```
#Definicion del job "despliegue_en_entorno_de_desarrollo",este job pertenece al segundo stage "despliegue" y se ejecuta en el entorno "desarrollo", este job realiza la instalacion  del programa necesario("dpl") para el correcto despliegue de la app en heroku y luego ejecuta dicho programa para poder desplegar el codigo ubicado en la rama DESARROLLO
despliegue_en_entorno_de_desarrollo: 
  environment:
  #se define el nombre del entorno
    name : desarrollo
  #si el job 'despliegue_en_entorno_de_desarrollo' se completa de forma satisfactoria, se habilita un boton 'en la parte superior' al lado del entorno("desarrollo")  que apunta a la app en desarrollo
    url  : http://gcc-tp2-2019-desarrollo.herokuapp.com
  #Define para que rama se ejecutara esta tarea, en este caso se ejecutara solo para los CAMBIOS QUE OCURRAN EN DESARROLLO
  only:
  - desarrollo
  script: 
    - "npm install"
    - "apt-get update -qy"
    #Instalacion de los programas necesarios para poder desplegar la app en heroku
    - "apt-get install -y ruby-dev"
    - "gem install dpl"
    #Utilizacion del programa necesario ("dpl") que permite desplegar la app en heroku
    - "dpl --provider=heroku --app=gcc-tp2-2019-desarrollo --api-key=$HEROKU_API_KEY"
  stage: despliegue

```
3. Tercer Job, denominado "despliegue_en_entorno_de_produccion", se ejecuta en el **Environment** de **produccion**. Este job se ejecuta despues de la ejecucion de los jobs que pertenezcan al primer stage (testeo), puesto que este job pertence al segundo stage(despliegue).**este job se ejecuta cuando se realizan merge/push solamente a la rama** master , este comportamiento se define con el **keyword "only"**  igualado a master(la rama master), ademas este job solo se ejecutara si los jobs precedentes (el job test) se ejecuta exitosamente, esto se configura con el **keyword "when"** definido a **"on_success"**. Al finalizar la ejecucion del job, aparecera un boton al lado del **Environment** en donde se ejecuto este job, y al oprimirlo **nos llevara a la url donde se encuentra deployada el codigo contenido en la rama master**, la url es : **"http://gcc-2019-tp2.herokuapp.com"** (**Diferente de la app de desarrollo**)


```
#Definicion del job "despliegue_en_entorno_de_produccion",este job pertenece al segundo stage "despliegue" y se ejecuta en el entorno "produccion", este job realiza la instalacion  del programa necesario("dpl") para el correcto despliegue de la app en heroku y luego ejecuta dicho programa para poder desplegar el codigo ubicado en la rama MASTER
despliegue_en_entorno_de_produccion:
#Definicion del entorno en donde se desplegar el codigo
  environment:
  #En este caso desplegara en el entorno de produccion
    name : produccion
    #si el job 'despliegue_en_entorno_de_produccion' se completa de forma satisfactoria, se habilita un boton 'en la parte superior' al lado del entorno("produccion")  que apunta a la app en desarrollo
    url : http://gcc-2019-tp2.herokuapp.com
  when: on_success
  #Se ejecuta solo ante cambios en la rama MASTER, merge request o bien push
  only:
  - master
  script:
  #Instalacion de los programas necesarios para deployar la app en heroku en produccion
    - "npm install"
    - "apt-get update -qy"
    - "apt-get install -y ruby-dev"
    - "gem install dpl"
    #Se utiliza el programa instalado ("dpl") para utilizarlo y poder desplegar la app en heroku
    - "dpl --provider=heroku --app=gcc-2019-tp2 --api-key=$HEROKU_API_KEY"
  #Pertence al stage despliegue    
  stage: despliegue
 



```

## Archivo .gitlab-ci.yml


```
image: "node:latest"

#Se define los stages,que son los pasos por los cuales pasara la rama ante cualquier cambio. a un stage pueden pertenecer varios jobs o task.
stages: 
  - testeo
  - despliegue
  
#Definicion del job "test", este job pertenece al primer stage "testeo", este job se realiza en el entorno "homologacion",dicho job ejecuta los scripts de instalacion de dependencias y tambien el script de test utilizando npm"
test:
  environment:
    name : homologacion
  script:
    - "npm install"
  #Se ejecuta la pruebas con mocha javascript
    - "npm test"
  stage: testeo

#Definicion del job "despliegue_en_entorno_de_desarrollo",este job pertenece al segundo stage "despliegue" y se ejecuta en el entorno "desarrollo", este job realiza la instalacion  del programa necesario("dpl") para el correcto despliegue de la app en heroku y luego ejecuta dicho programa para poder desplegar el codigo ubicado en la rama DESARROLLO
despliegue_en_entorno_de_desarrollo: 
  environment:
  #se define el nombre del entorno
    name : desarrollo
  #si el job 'despliegue_en_entorno_de_desarrollo' se completa de forma satisfactoria, se habilita un boton 'en la parte superior' al lado del entorno("desarrollo")  que apunta a la app en desarrollo
    url  : http://gcc-tp2-2019-desarrollo.herokuapp.com
  #Define para que rama se ejecutara esta tarea, en este caso se ejecutara solo para los CAMBIOS QUE OCURRAN EN DESARROLLO
  only:
  - desarrollo
  script: 
    - "npm install"
    - "apt-get update -qy"
    #Instalacion de los programas necesarios para poder desplegar la app en heroku
    - "apt-get install -y ruby-dev"
    - "gem install dpl"
    #Utilizacion del programa necesario ("dpl") que permite desplegar la app en heroku
    - "dpl --provider=heroku --app=gcc-tp2-2019-desarrollo --api-key=$HEROKU_API_KEY"
  stage: despliegue
  
#Definicion del job "despliegue_en_entorno_de_produccion",este job pertenece al segundo stage "despliegue" y se ejecuta en el entorno "produccion", este job realiza la instalacion  del programa necesario("dpl") para el correcto despliegue de la app en heroku y luego ejecuta dicho programa para poder desplegar el codigo ubicado en la rama MASTER
despliegue_en_entorno_de_produccion:
#Definicion del entorno en donde se desplegar el codigo
  environment:
  #En este caso desplegara en el entorno de produccion
    name : produccion
    #si el job 'despliegue_en_entorno_de_produccion' se completa de forma satisfactoria, se habilita un boton 'en la parte superior' al lado del entorno("produccion")  que apunta a la app en desarrollo
    url : http://gcc-2019-tp2.herokuapp.com
  when: on_success
  #Se ejecuta solo ante cambios en la rama MASTER, merge request o bien push
  only:
  - master
  script:
  #Instalacion de los programas necesarios para deployar la app en heroku en produccion
    - "npm install"
    - "apt-get update -qy"
    - "apt-get install -y ruby-dev"
    - "gem install dpl"
    #Se utiliza el programa instalado ("dpl") para utilizarlo y poder desplegar la app en heroku
    - "dpl --provider=heroku --app=gcc-2019-tp2 --api-key=$HEROKU_API_KEY"
  #Pertence al stage despliegue    
  stage: despliegue



```