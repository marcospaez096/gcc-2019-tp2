var express = require('express');
var session = require('express-session');
var bodyParser = require('body-parser');
var path = require('path');
var fs = require('fs');

var app = express();

app.use(session({
	secret: 'secret',
	resave: true,
	saveUninitialized: true
}));


app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());


app.get('/', function(request, response) {
	response.sendFile(path.join(__dirname + '/login.html'));
});


app.get('/home', function(request, response) {
	if (request.session.loggedin) {
		response.status(200).send('<h2>Bienvenido!, ' + request.session.username + '!</h2>');
	} else {
		response.status(403).send('Please login to view this page!');
	}
	response.end();
});



app.post('/auth', function(request, response) {
	var username = request.body.username;
	var password = request.body.password;
	var users;
	var flag = 0;
	if (username && password) {
		console.log('Se enviaron el username y el password');
		fs.readFile(__dirname + '/../files/users.json', 'utf8', function(err, fileContents) {
					if(err)  throw err;
					users = JSON.parse(fileContents);
					if (users["usuarios"].filter(e => e["username"] === username && e["password"]==password).length > 0) {
							request.session.loggedin = true;
							request.session.username = username;
							response.redirect('/home');
 
					}else{
							response.status(403).send(' Username Y/O Password! incorrecto');
							response.end();
					}

		});
			
	} else {
		response.send('Por favor ingrese Username y Password!');
		response.end();
	}
});



app.listen(process.env.PORT || 3000)
console.log("Listening!");
