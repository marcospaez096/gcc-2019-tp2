
var assert = require("assert");
var fs = require("fs");
var request = require('supertest')
var app = require('../app/login.js');


describe('Unit Testing', function () {
	var users;
	var user = {"username" : "test" , "password" : "test1" };
	//Antes de ejecutar el test se revisa si existe el archivo
	before(function(done){
		console.log("Verificando si existe el archivo 'users.json' archivo antes de ir al test");
		fs.readFile('./files/users.json', 'utf8', function(err, fileContents) {
					if(err) throw err;
					users = JSON.parse(fileContents);
					done();
					
		});

	
	});
	//Ir al test si existe el archivo
	it('Deberia existir el usuario especificado', function (done) {
			
			users["usuarios"].forEach(function(element){
					
					if(element["username"] == user["username"] && element["password"] == user["password"]){

						assert.ok(true);
						done();
						
					
					}


			
			});


			throw new Error("No existe el usuario");
			done();

				
				
				
		
		
		
	});
	it('Deberia redireccionar a home', function(done){

             let user = {"username" : "test", "password" : "test1"};

             request("http://localhost:3000").post('/auth')
                 .send(user)
		  .expect(302)
              	  .expect('Location', '/home')
          	.end(done)             
         });			
	
	
});


